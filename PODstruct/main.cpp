#include <iostream>
#include <fstream>
#include <cstdint>

enum class EepromFormat {	FORMAT1 = 0, 	FORMAT2 = 1};
const char* getFormatName[] = {	"FORMAT1", 	"FORMAT2"};

struct EepromDataStruct {
    EepromFormat format;
    uint64_t serialNumber;
    uint8_t productName[20];
    uint8_t companyName[20];
};

int main()
{	
	EepromDataStruct myEeprom;
	std::ifstream in("BinaryEeprom.txt", std::ios::binary|std::ios::in);
	in.read(reinterpret_cast<char *>(&myEeprom.format), 		sizeof(myEeprom.format));
	in.read(reinterpret_cast<char *>(&myEeprom.serialNumber), 	sizeof(myEeprom.serialNumber));
	in.read(reinterpret_cast<char *>(myEeprom.productName), 	sizeof(myEeprom.productName));
	in.read(reinterpret_cast<char *>(myEeprom.companyName), 	sizeof(myEeprom.companyName));
	std::cout	<<"format\t"	<<getFormatName[static_cast<int>(myEeprom.format)]	<<std::endl
			<<"serialNumber\t"<<myEeprom.serialNumber					<<std::endl
			<<"productName\t"	<<myEeprom.productName					<<std::endl
			<<"companyName\t"	<<myEeprom.companyName					<<std::endl;
	return 0;
}
