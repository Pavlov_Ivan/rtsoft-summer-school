/* Dummy character device driver from Free Electrons
 * Referenced in http://free-electrons.com/training/drivers
 *
 * Copyright 2006, Free Electrons <http://free-electrons.com>
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/io.h>
#include <asm/uaccess.h>
#include <linux/wait.h>
#include <linux/sched.h>

/* Physical address of the imaginary myipc device */
//#define myipc_PHYS 0x12345678

#define myipc_bufsize 8192

static char myipc_buf[myipc_bufsize]="";

static int myipc_count = 1;
static dev_t myipc_dev = MKDEV(203, 128);

static struct cdev myipc_cdev;
//static int is_read = 1;
static size_t pread = 0;
static size_t pwrite = 0;

static DECLARE_WAIT_QUEUE_HEAD(wq_wr);
static DECLARE_WAIT_QUEUE_HEAD(wq_rd);

static ssize_t
myipc_read(struct file *file, char __user * buf, size_t count, loff_t * ppos)
{
	/* The myipc_buf address corresponds to a device I/O memory area */
	/* of size myipc_bufsize, obtained with ioremap() */
	int transfer_size;

	printk("Read Called\n");
	printk(myipc_buf);
	printk("rd %u wr%u", (unsigned int)pread, (unsigned int)pwrite);
	//if (is_read) {
	//	return -EAGAIN;
	//}
				/* bytes left to transfer */
	//if (remaining_size == 0) {
				/* All read, returning 0 (End Of File) */
	//	return 0;
	//}

	/* Size of this transfer */
	transfer_size = min_t(size_t, pwrite-pread, count);
	if(0 == transfer_size) {
	//	wait_event_interruptible
	//		(wq_rd, 0 != (transfer_size = min_t(size_t, pwrite-pread, count)));
		return -EAGAIN;
	}
	if (copy_to_user
		(buf /* to */ ,
		 &myipc_buf[pread % myipc_bufsize] /* from */ ,
		 transfer_size)) {
		return -EFAULT;
	} else {		/* Increase the position in the open file */
		//*ppos += transfer_size;
		//is_read = 1;
		pread += transfer_size;
		wake_up_interruptible(&wq_wr);
		return transfer_size;
	}
}

static ssize_t
myipc_write(struct file *file, const char __user *buf, size_t count,
	   loff_t *ppos)
{
	printk("Write Called");
	printk(myipc_buf);
	printk("rd %u wr %u", (unsigned int)pread, (unsigned int)pwrite);
	if (count > myipc_bufsize) {
		/* Can't write beyond the end of the device */
		return -EIO;
	}
	if (!(pwrite+count<pread+myipc_bufsize)) {
		wait_event_interruptible
			(wq_wr, pwrite+count<pread+myipc_bufsize);
		//return -EAGAIN;
	}
	if (copy_from_user(&myipc_buf[pwrite % myipc_bufsize] /*to*/ , 
			buf /*from*/ , 
			count)) {
		return -EFAULT;
	} else {
		/* Increase the position in the open file */
		//is_read = 0;
		//*ppos += count;
		pwrite+=count;
		//wake_up_interruptible(&wq_rd);	
		return count;
	}
}

static const struct file_operations myipc_fops = {
	.owner = THIS_MODULE,
	.read = myipc_read,
	.write = myipc_write,
};

static int __init myipc_init(void)
{
	int err;
	//myipc_buf = ioremap(myipc_PHYS, myipc_bufsize);

	/*if (!myipc_buf) {
		err = -ENOMEM;
		goto err_exit;
	}*/
	
	if (register_chrdev_region(myipc_dev, myipc_count, "myipc")) {
		err = -ENODEV;
		goto err_free_buf;
	}

	cdev_init(&myipc_cdev, &myipc_fops);

	if (cdev_add(&myipc_cdev, myipc_dev, myipc_count)) {
		err = -ENODEV;
		goto err_dev_unregister;
	}

	return 0;

 err_dev_unregister:
	unregister_chrdev_region(myipc_dev, myipc_count);
 err_free_buf:
	return 0;
	//iounmap(myipc_buf);
// err_exit:
//	return err;
}

static void __exit myipc_exit(void)
{
	cdev_del(&myipc_cdev);
	unregister_chrdev_region(myipc_dev, myipc_count);
	//iounmap(myipc_buf);
}

module_init(myipc_init);
module_exit(myipc_exit);
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Example character driver");
MODULE_AUTHOR("Free Electrons and Pavlov Ivan");
