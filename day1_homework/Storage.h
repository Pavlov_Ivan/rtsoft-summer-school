#pragma once
#include <cstdint>
#include <cstddef>

// Если Вы ещё не читали код и комментарии в файле "MyVector.h", начните с них.
// Иначе, вот продолжение истории:

// Василий попытался описать структуру данных, выделяющую некоторое количество памяти на стеке.
template <int NBytes>
class StackStorage
{
public:
	std::uint8_t data_[NBytes];
};

// Также Василий знает, что ему нельзя использовать исключения, поэтому
// результат выделения памяти нужно будет возвращать через код ошибки.
// Василий недавно прошёл тренинг по C++11, поэтому знает о таких клёвых фичах,
// как scoped enum
enum class MemoryAllocationResult
{
	Ok,
	OutOfMemory,
	UnexpectedFailure
};

// Дальше Василий пишет класс для управления памятью на куче.
// Правильно ли? Он до конца не уверен. Но времени разбираться нет, начальство подгоняет...
class HeapStorage
{
public:
	explicit HeapStorage(size_t size = 0):	data_(nullptr), size_(0)
	{
		Reallocate(size);
	}

	MemoryAllocationResult Reallocate(size_t size)
	{
		if (size > 0) 
		{
			std::uint8_t* newData = new(std::nothrow) std::uint8_t[size];
			if (nullptr != newData) 
			{
				if (0 != size_)				
					std::copy(data_, data_ + size_, newData);
				Deallocate();
				data_ = newData;
				size_ = size;
				return MemoryAllocationResult::Ok;
			}
			else 
			{
				return MemoryAllocationResult::OutOfMemory;
			}
		}
		else
		{
			return MemoryAllocationResult::UnexpectedFailure;
		}
	}

	void Deallocate()
	{	
		size_ = 0;
		if(0 != size_)		
			delete[] data_;
	}
	
	~HeapStorage()
	{
		Deallocate();
	}
	std::uint8_t* GetData() const { return data_; }
	std::uint32_t GetSize() const { return size_; }
private:
	std::uint32_t size_;
	std::uint8_t* data_;
};

// ...так что Василий продолжает дальше
// и создаёт статические буферы памяти
constexpr size_t numBytesToFitInStack = 1000;
static StackStorage<numBytesToFitInStack> stackStorage;
static HeapStorage heapStorage{ 0 };

// Но здесь, как водится, его отвлекают на другой, более "горящий проект",
// и Василий не успевает переписать класс MyVector в соответствии с поставленными требованиями.

// Василий очень просит вас помочь ему сладить с этой задачей.
