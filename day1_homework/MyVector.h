#pragma once

#include <stdexcept>
#include <string>
#include <assert.h>
#include "Storage.h"

// Как и любой программист, Василий знает, что за свою жизнь он обязан сделать 3 вещи:
// - посадить зрение 
// - изобрести велосипед
// - вырастить репозиторий
//
// Решив начать с велосипеда, он стал писать свой класс вектора для своих собственных
// embedded-примений - в самом деле, все ведь знают, что STL контейнеры не годятся для embedded устройств.
// Этот класс он написал по всем канонам, в точности как его учили на занятиях по C++,
// с применением RAII-идиомы и проверками на выход границ массива.

class MyVectorOutOfRange : public std::runtime_error
{
public:
	explicit MyVectorOutOfRange(int invalidIndex)
		: runtime_error(std::to_string(invalidIndex) + " - invalid index")
	{
	}

	char const* what() const noexcept override
	{
		return runtime_error::what();
	}
};

class MyVector
{
public:
	// Однако - ой! оказалось, что вектор Василия должен уметь динамически менять размер.
	// Причём как расти, так и "сжиматься".
	void Resize(size_t newSize)
	{
		if (newSize <= sizeof(stackStorage.data_))
		{
			if (!isInStack)
			{
				if (0 != numElements_)
					std::copy(data_, data_ + newSize, stackStorage.data_);
				data_ = stackStorage.data_;
				isInStack = true;
				heapStorage.Deallocate();
			}
			numElements_ = newSize;
		}
		else 
		{
			auto result = heapStorage.Reallocate(newSize);
			if (MemoryAllocationResult::Ok == result)
			{
				if (isInStack)
				{
					if (0 != numElements_)
						std::copy(data_, data_ + numElements_, heapStorage.GetData());
					data_ = heapStorage.GetData();
					isInStack = false;
				}
				numElements_ = newSize;
				//assert (0);
			}
			else
			{
				//memory allocation error
			}
		}
	}

	// Другой "ой" вышел с исключениями - выяснилось, что на любимой embedded-платформе Василия
	// использовать их ну никак нельзя.
	// "Уж не придётся ли мне менять интерфейс моих методов GetAt и SetAt?" - подумал Василий.
	uint8_t GetAt(size_t index)
	{
		if (index > numElements_)
		{
			return -1;
			//throw MyVectorOutOfRange(index);
		}
		return data_[index];
	}

	void SetAt(size_t index, int value)
	{
		if (index > numElements_)
		{
			return ;
			//throw MyVectorOutOfRange(index);
		}
		data_[index] = value;
	}
	size_t GetSize() { return numElements_; }
	// Вдобавок ко всему оказалось, что динамическая аллокация памяти в куче на целевой платформе
	// крайне нежелательна. 
	// Вместо этого хочется выделять память из пред-выделенного буфера.
	// А ещё лучше использовать память на стеке. Но её может не хватить.
	// Поэтому хочется использовать стек, когда можно, и буфер на куче, когда нельзя.
	explicit MyVector(size_t numElements)
		: numElements_(0), data_(stackStorage.data_), isInStack(true)
	{
		Resize(numElements);
	}

	~MyVector()
	{
		heapStorage.Deallocate();
	}

private:
	uint8_t* data_;
	size_t numElements_;
	bool isInStack;
};

// В попытке подстроить задачу под требования проекта Василий написал ещё пару классов
// (см. header-файл "Storage.h")
