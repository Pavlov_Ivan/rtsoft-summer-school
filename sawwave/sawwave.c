/* Dummy character device driver from Free Electrons
 * Referenced in http://free-electrons.com/training/drivers
 *
 * Copyright 2006, Free Electrons <http://free-electrons.com>
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/io.h>
#include <asm/uaccess.h>
#include <linux/wait.h>
#include <linux/sched.h>
#include <linux/kthread.h>
#include <linux/delay.h>

/* Physical address of the imaginary sawwave device */
//#define sawwave_PHYS 0x12345678

#define sawwave_bufsize 8192

static struct task_struct *ts;

static char sawwave_buf[sawwave_bufsize]="";

static int sawwave_count = 1;
static dev_t sawwave_dev = MKDEV(204, 128);

static struct cdev sawwave_cdev;
//static int is_read = 1;
static size_t pread = 0;
static size_t pwrite = 0;

static DECLARE_WAIT_QUEUE_HEAD(wq_wr);
static DECLARE_WAIT_QUEUE_HEAD(wq_rd);

//static struct mutex buf_mutex;
//mutex_init(&buf_mutex);

static DEFINE_MUTEX(buf_mutex);

static int thread_func(void *data)
{
	unsigned char n = 0;
	while(true)
	{
		mutex_lock(&buf_mutex);
		sawwave_buf[pwrite%sawwave_bufsize] = '0' + n;
		++pwrite;
		mutex_unlock(&buf_mutex);
        if(pwrite%16==0)
            wake_up_interruptible(&wq_rd);
		msleep(100);
		n = (n+1)%10;
	}
	return 0;
}

static size_t get_transfer_size(size_t count)
{
    return min_t(size_t, 
            pread % sawwave_bufsize + pwrite-pread <= sawwave_bufsize?
            pwrite-pread:
            sawwave_bufsize - pread % sawwave_bufsize, 
            count);
}

static ssize_t
sawwave_read(struct file *file, char __user * buf, size_t count, loff_t * ppos)
{
	int transfer_size;

	printk("Read Called\n");
	printk(sawwave_buf);
	printk("rd %u wr %u", (unsigned int)pread, (unsigned int)pwrite);

	mutex_lock(&buf_mutex);
	transfer_size = get_transfer_size(count);//min_t(size_t, pwrite-pread<, count);
	if(0 == transfer_size) {	
		mutex_unlock(&buf_mutex);	
	        wait_event_interruptible
	    		(wq_rd, 0 != (transfer_size = get_transfer_size(count)));
	        mutex_lock(&buf_mutex);
		//return -EAGAIN;
	}
	if (copy_to_user
		(buf /* to */ ,
		 &sawwave_buf[pread % sawwave_bufsize] /* from */ ,
		 transfer_size)) {
		mutex_unlock(&buf_mutex);
		return -EFAULT;
	} else {		/* Increase the position in the open file */
		pread += transfer_size;
		//wake_up_interruptible(&wq_wr);
		mutex_unlock(&buf_mutex);
		return transfer_size;
	}
}

static const struct file_operations sawwave_fops = {
	.owner = THIS_MODULE,
	.read = sawwave_read,
};

static int __init sawwave_init(void)
{
	if (register_chrdev_region(sawwave_dev, sawwave_count, "sawwave")) {
		return -ENODEV;
	}

	cdev_init(&sawwave_cdev, &sawwave_fops);

	if (cdev_add(&sawwave_cdev, sawwave_dev, sawwave_count)) {
		unregister_chrdev_region(sawwave_dev, sawwave_count);
		return -ENODEV;
	}
	ts = kthread_run(&thread_func, NULL, "Our thread");	
	return 0;
}

static void __exit sawwave_exit(void)
{
	cdev_del(&sawwave_cdev);
	unregister_chrdev_region(sawwave_dev, sawwave_count);
	kthread_stop(ts);
}

module_init(sawwave_init);
module_exit(sawwave_exit);
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Example character driver");
MODULE_AUTHOR("Free Electrons and Pavlov Ivan");
